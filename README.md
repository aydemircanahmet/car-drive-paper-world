<ENG>
Meteor v Rope Privacy Policy
-- What have been added on Version 1.0.1
> This app uses Facebook SDK. It means we collect your session length, session count data.
-- Version 1.0
> This game connects to GameCenter & sends score to leaderboard.
> This app contains in-app purchases.
> This aps contains 3rd party advertisements.
</ENG>

<ENG>
One Last Car Fantasy Privacy Policy
> Our app doesn't collect, share or save any personal data.
> This app contains 3rd party advertisements.
> This app contains in-app purchases.
</ENG>

<ENG>
A Journey to Spiral World Privacy Policy
> This app collects the data of how much and how often you play it. For more information, check Voodoo Privacy Policy.
</ENG>

<ENG>
Car Drive - Paper World Privacy Policy
> Our app doesn't collect, share or save any personal data.
> This app doesn't contain 3rd party advertisements.
</ENG>

<ENG> 
Animight - Forrest League Privacy Policy
> Our app doesn't collect, share or save any personal data.
> This app doesn't contain 3rd party advertisements.
</ENG>